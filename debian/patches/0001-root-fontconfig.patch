From c5f13cb25c3b803346d1a6cac483badbcf2ced62 Mon Sep 17 00:00:00 2001
From: Mattias Ellert <mattias.ellert@physics.uu.se>
Date: Sat, 11 May 2024 20:09:47 +0200
Subject: [PATCH] Use system fonts via fontconfig

---
 core/base/src/TApplication.cxx        |  11 +-
 graf2d/asimage/CMakeLists.txt         |   1 +
 graf2d/asimage/src/TASImage.cxx       |  91 +++++++-
 graf2d/graf/CMakeLists.txt            |   1 +
 graf2d/graf/inc/TTF.h                 |   3 +-
 graf2d/graf/src/TTF.cxx               | 291 +++++++++++++++++---------
 graf2d/postscript/CMakeLists.txt      |   1 +
 graf2d/postscript/src/TPostScript.cxx | 106 +++++-----
 graf3d/gl/CMakeLists.txt              |   1 +
 graf3d/gl/src/TGLFontManager.cxx      | 123 ++++++++++-
 graf3d/gl/src/TGLText.cxx             |  56 +++--
 gui/gui/src/TGApplication.cxx         |  10 +-
 12 files changed, 494 insertions(+), 201 deletions(-)

--- a/core/base/src/TApplication.cxx
+++ b/core/base/src/TApplication.cxx
@@ -251,18 +251,12 @@
       LoadGraphicsLibs();
 
       // Try to load TrueType font renderer. Only try to load if not in batch
-      // mode and Root.UseTTFonts is true and Root.TTFontPath exists. Abort silently
+      // mode and Root.UseTTFonts is true. Abort silently
       // if libttf or libGX11TTF are not found in $ROOTSYS/lib or $ROOTSYS/ttf/lib.
-      const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                          TROOT::GetTTFFontDir());
-      char *ttfont = gSystem->Which(ttpath, "arialbd.ttf", kReadPermission);
-      // Check for use of DFSG - fonts
-      if (!ttfont)
-         ttfont = gSystem->Which(ttpath, "FreeSansBold.ttf", kReadPermission);
 
    #if !defined(R__WIN32)
       if (!gROOT->IsBatch() && !strcmp(gVirtualX->GetName(), "X11") &&
-          ttfont && gEnv->GetValue("Root.UseTTFonts", 1)) {
+          gEnv->GetValue("Root.UseTTFonts", 1)) {
          if (gClassTable->GetDict("TGX11TTF")) {
             // in principle we should not have linked anything against libGX11TTF
             // but with ACLiC this can happen, initialize TGX11TTF by hand
@@ -276,7 +270,6 @@
          }
       }
    #endif
-      delete [] ttfont;
    }
 
    if (!only_web || !fAppImp) {
--- a/graf2d/asimage/CMakeLists.txt
+++ b/graf2d/asimage/CMakeLists.txt
@@ -30,6 +30,7 @@
     ${FREETYPE_LIBRARIES}
     ${X11_LIBRARIES}
     ZLIB::ZLIB
+    fontconfig
   DEPENDENCIES
     Core
     Graf
--- a/graf2d/asimage/src/TASImage.cxx
+++ b/graf2d/asimage/src/TASImage.cxx
@@ -117,6 +117,8 @@
 #   include <draw.h>
 }
 
+#include <fontconfig/fontconfig.h>
+
 // auxiliary functions for general polygon filling
 #include "TASPolyUtils.c"
 
@@ -2595,11 +2597,88 @@
    // This is for backward compatibility...
    if (fn.Last('/') == 0) fn = fn(1, fn.Length() - 1);
 
-   const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                       TROOT::GetTTFFontDir());
-   char *tmpstr = gSystem->Which(ttpath, fn, kReadPermission);
-   fn = tmpstr;
-   delete [] tmpstr;
+   const char *basename = gSystem->BaseName(fn);
+
+   int ttindex = 0;
+
+   FcPattern *pat = nullptr, *match;
+   FcResult result;
+
+   if (strcmp(basename, "timesi.ttf") == 0 ||
+       strcmp(basename, "FreeSerifItalic.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif:italic");
+   }
+   else if (strcmp(basename, "timesbd.ttf") == 0 ||
+            strcmp(basename, "FreeSerifBold.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif:bold");
+   }
+   else if (strcmp(basename, "timesbi.ttf") == 0 ||
+            strcmp(basename, "FreeSerifBoldItalic.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif:bold:italic");
+   }
+   else if (strcmp(basename, "arial.ttf") == 0 ||
+            strcmp(basename, "FreeSans.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans");
+   }
+   else if (strcmp(basename, "ariali.ttf") == 0 ||
+            strcmp(basename, "FreeSansOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans:italic");
+   }
+   else if (strcmp(basename, "arialbd.ttf") == 0 ||
+            strcmp(basename, "FreeSansBold.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans:bold");
+   }
+   else if (strcmp(basename, "arialbi.ttf") == 0 ||
+            strcmp(basename, "FreeSansBoldOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans:bold:italic");
+   }
+   else if (strcmp(basename, "cour.ttf") == 0 ||
+            strcmp(basename, "FreeMono.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono");
+   }
+   else if (strcmp(basename, "couri.ttf") == 0 ||
+            strcmp(basename, "FreeMonoOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono:italic");
+   }
+   else if (strcmp(basename, "courbd.ttf") == 0 ||
+            strcmp(basename, "FreeMonoBold.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono:bold");
+   }
+   else if (strcmp(basename, "courbi.ttf") == 0 ||
+            strcmp(basename, "FreeMonoBoldOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono:bold:italic");
+   }
+   else if (strcmp(basename, "symbol.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "standardsymbolsps");
+   }
+   else if (strcmp(basename, "times.ttf") == 0 ||
+            strcmp(basename, "FreeSerif.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif");
+   }
+   else if (strcmp(basename, "wingding.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "dingbats");
+   }
+   else if (strcmp(basename, "BlackChancery.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "urwchanceryl");
+   }
+   else if (gSystem->AccessPathName(fn, kReadPermission) != 0) {
+      // Font name is not a file - usae as pattern
+      pat = FcNameParse ((const FcChar8*) fn.Data());
+   }
+
+   if (pat) {
+      FcConfigSubstitute (nullptr, pat, FcMatchPattern);
+      FcDefaultSubstitute (pat);
+      match = FcFontMatch (nullptr, pat, &result);
+      if (match) {
+         char *ttfnt;
+         FcPatternGetString (match, FC_FILE, 0, (FcChar8**) &ttfnt);
+         fn = ttfnt;
+         FcPatternGetInteger (match, FC_INDEX, 0, &ttindex);
+         FcPatternDestroy (match);
+      }
+      FcPatternDestroy (pat);
+   }
 
    if (fn.EndsWith(".pfa") || fn.EndsWith(".PFA") || fn.EndsWith(".pfb") || fn.EndsWith(".PFB") || fn.EndsWith(".ttf") || fn.EndsWith(".TTF") || fn.EndsWith(".otf") || fn.EndsWith(".OTF")) {
       ttfont = kTRUE;
@@ -2623,7 +2702,7 @@
       return;
    }
 
-   ASFont *font = get_asfont(gFontManager, fn.Data(), 0, size, ASF_GuessWho);
+   ASFont *font = get_asfont(gFontManager, fn.Data(), ttindex, size, ASF_GuessWho);
 
    if (!font) {
       font = get_asfont(gFontManager, "fixed", 0, size, ASF_GuessWho);
--- a/graf2d/graf/CMakeLists.txt
+++ b/graf2d/graf/CMakeLists.txt
@@ -91,6 +91,7 @@
     ${FREETYPE_LIBRARIES}
     ZLIB::ZLIB
     mathtext
+    fontconfig
   DEPENDENCIES
     Hist
     Matrix
--- a/graf2d/graf/inc/TTF.h
+++ b/graf2d/graf/inc/TTF.h
@@ -76,9 +76,10 @@
    static FT_BBox        fgCBox;                  ///< string control box
    static FT_CharMap     fgCharMap[kTTMaxFonts];  ///< font character map
    static Int_t          fgCurFontIdx;            ///< current font index
-   static Int_t          fgSymbItaFontIdx;        ///< Symbol italic font index
    static Int_t          fgFontCount;             ///< number of fonts loaded
    static char          *fgFontName[kTTMaxFonts]; ///< font name
+   static Int_t          fgFontIdx[kTTMaxFonts];  ///< font index in font file
+   static Int_t          fgFontIta[kTTMaxFonts];  ///< font slanted using transform
    static FT_Face        fgFace[kTTMaxFonts];     ///< font face
    static TTF::TTGlyph   fgGlyphs[kMaxGlyphs];    ///< glyphs
    static Bool_t         fgHinting;               ///< use hinting (true by default)
--- a/graf2d/graf/src/TTF.cxx
+++ b/graf2d/graf/src/TTF.cxx
@@ -25,6 +25,8 @@
 #include "TMath.h"
 #include "TError.h"
 
+#include <fontconfig/fontconfig.h>
+
 // to scale fonts to the same size as the old TT version
 const Float_t kScale = 0.93376068;
 
@@ -38,10 +40,11 @@
 Int_t          TTF::fgWidth          = 0;
 Int_t          TTF::fgAscent         = 0;
 Int_t          TTF::fgCurFontIdx     = -1;
-Int_t          TTF::fgSymbItaFontIdx = -1;
 Int_t          TTF::fgFontCount      = 0;
 Int_t          TTF::fgNumGlyphs      = 0;
 char          *TTF::fgFontName[kTTMaxFonts];
+Int_t          TTF::fgFontIdx[kTTMaxFonts];
+Int_t          TTF::fgFontIta[kTTMaxFonts];
 FT_Matrix     *TTF::fgRotMatrix      = nullptr;
 FT_Library     TTF::fgLibrary;
 FT_BBox        TTF::fgCBox;
@@ -72,6 +75,11 @@
       return;
    }
 
+   // Add root's font directory
+   const char *ttpath = gEnv->GetValue("Root.TTFontPath",
+                                       TROOT::GetTTFFontDir());
+   FcConfigAppFontAddDir (nullptr, (const FcChar8*)ttpath);
+
    // load default font (arialbd)
    SetTextFont(62);
 }
@@ -109,12 +117,15 @@
          charmap  = fgFace[fgCurFontIdx]->charmaps[i];
          platform = charmap->platform_id;
          encoding = charmap->encoding_id;
-         if ((platform == 3 && encoding == 1) ||
+         if ((platform == 3 && encoding == 1 &&
+              (fgFontIta[fgCurFontIdx] & 2) == 0) ||
              (platform == 0 && encoding == 0) ||
+             (platform == 7 && encoding == 2 &&
+              (fgFontIta[fgCurFontIdx] & 2) != 0) ||
+             (platform == 0 && encoding == 3 &&
+              (fgFontIta[fgCurFontIdx] & 2) != 0) ||
              (platform == 1 && encoding == 0 &&
-              !strcmp(fgFontName[fgCurFontIdx], "wingding.ttf")) ||
-             (platform == 1 && encoding == 0 &&
-              !strcmp(fgFontName[fgCurFontIdx], "symbol.ttf")))
+              (fgFontIta[fgCurFontIdx] & 2) != 0))
          {
             fgCharMap[fgCurFontIdx] = charmap;
             if (FT_Set_Charmap(fgFace[fgCurFontIdx], fgCharMap[fgCurFontIdx]))
@@ -387,21 +398,145 @@
    }
    const char *basename = gSystem->BaseName(fontname);
 
+   char *ttfont = nullptr;
+   int ttindex = 0;
+
+   FcPattern *pat = nullptr, *match;
+   FcResult result;
+
+   if (strcmp(basename, "timesi.ttf") == 0 ||
+       strcmp(basename, "FreeSerifItalic.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif:italic");
+   }
+   else if (strcmp(basename, "timesbd.ttf") == 0 ||
+            strcmp(basename, "FreeSerifBold.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif:bold");
+   }
+   else if (strcmp(basename, "timesbi.ttf") == 0 ||
+            strcmp(basename, "FreeSerifBoldItalic.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif:bold:italic");
+   }
+   else if (strcmp(basename, "arial.ttf") == 0 ||
+            strcmp(basename, "FreeSans.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans");
+   }
+   else if (strcmp(basename, "ariali.ttf") == 0 ||
+            strcmp(basename, "FreeSansOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans:italic");
+   }
+   else if (strcmp(basename, "arialbd.ttf") == 0 ||
+            strcmp(basename, "FreeSansBold.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans:bold");
+   }
+   else if (strcmp(basename, "arialbi.ttf") == 0 ||
+            strcmp(basename, "FreeSansBoldOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freesans:bold:italic");
+   }
+   else if (strcmp(basename, "cour.ttf") == 0 ||
+            strcmp(basename, "FreeMono.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono");
+   }
+   else if (strcmp(basename, "couri.ttf") == 0 ||
+            strcmp(basename, "FreeMonoOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono:italic");
+   }
+   else if (strcmp(basename, "courbd.ttf") == 0 ||
+            strcmp(basename, "FreeMonoBold.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono:bold");
+   }
+   else if (strcmp(basename, "courbi.ttf") == 0 ||
+            strcmp(basename, "FreeMonoBoldOblique.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freemono:bold:italic");
+   }
+   else if (strcmp(basename, "symbol.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "standardsymbolsps");
+      italic &= 2;
+   }
+   else if (strcmp(basename, "times.ttf") == 0 ||
+            strcmp(basename, "FreeSerif.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "freeserif");
+   }
+   else if (strcmp(basename, "wingding.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "dingbats");
+      italic &= 2;
+   }
+   else if (strcmp(basename, "STIXGeneral.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixgeneral");
+   }
+   else if (strcmp(basename, "STIXGeneralItalic.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixgeneral:italic");
+   }
+   else if (strcmp(basename, "STIXGeneralBol.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixgeneral:bold");
+   }
+   else if (strcmp(basename, "STIXGeneralBolIta.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixgeneral:bold:italic");
+   }
+   else if (strcmp(basename, "STIXSiz1Sym.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizeonesym");
+   }
+   else if (strcmp(basename, "STIXSiz1SymBol.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizeonesym:bold");
+   }
+   else if (strcmp(basename, "STIXSiz2Sym.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizetwosym");
+   }
+   else if (strcmp(basename, "STIXSiz2SymBol.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizetwosym:bold");
+   }
+   else if (strcmp(basename, "STIXSiz3Sym.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizethreesym");
+   }
+   else if (strcmp(basename, "STIXSiz3SymBol.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizethreesym:bold");
+   }
+   else if (strcmp(basename, "STIXSiz4Sym.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizefoursym");
+   }
+   else if (strcmp(basename, "STIXSiz4SymBol.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizefoursym:bold");
+   }
+   else if (strcmp(basename, "STIXSiz5Sym.otf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "stixsizefivesym");
+   }
+   else if (strcmp(basename, "DroidSansFallback.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "droidsansfallback:charset=4e00 0410");
+   }
+   else if (strcmp(basename, "BlackChancery.ttf") == 0) {
+      pat = FcNameParse ((const FcChar8*) "urwchanceryl");
+   }
+   else if (gSystem->AccessPathName(fontname, kReadPermission) != 0) {
+      // Font name is not a file - usae as pattern
+      pat = FcNameParse ((const FcChar8*) fontname);
+   }
+
+   if (pat) {
+      FcConfigSubstitute (nullptr, pat, FcMatchPattern);
+      FcDefaultSubstitute (pat);
+      match = FcFontMatch (nullptr, pat, &result);
+      if (match) {
+         const char *ttfnt;
+         FcPatternGetString (match, FC_FILE, 0, (FcChar8**) &ttfnt);
+         ttfont = StrDup(ttfnt);
+         FcPatternGetInteger (match, FC_INDEX, 0, &ttindex);
+         FcPatternDestroy (match);
+      }
+      FcPatternDestroy (pat);
+   }
+
+   if (!ttfont) ttfont = StrDup(fontname);
+
+   basename = gSystem->BaseName(ttfont);
+
    // check if font is in cache
    int i;
    for (i = 0; i < fgFontCount; i++) {
-      if (!strcmp(fgFontName[i], basename)) {
-         if (italic) {
-            if (i==fgSymbItaFontIdx) {
-               fgCurFontIdx = i;
-               return 0;
-            }
-         } else {
-            if (i!=fgSymbItaFontIdx) {
-               fgCurFontIdx = i;
-               return 0;
-            }
-         }
+      if (!strcmp(fgFontName[i], basename) &&
+          (fgFontIdx[i] == ttindex) &&
+          (fgFontIta[i] == italic)) {
+         fgCurFontIdx = i;
+         delete [] ttfont;
+         return 0;
       }
    }
 
@@ -411,28 +546,13 @@
             kTTMaxFonts);
       Warning("TTF::SetTextFont", "using default font %s", fgFontName[0]);
       fgCurFontIdx = 0;    // use font 0 (default font, set in ctor)
+      delete [] ttfont;
       return 0;
    }
 
-   // try to load font (font must be in Root.TTFontPath resource)
-   const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                       TROOT::GetTTFFontDir());
-   char *ttfont = gSystem->Which(ttpath, fontname, kReadPermission);
-
-   if (!ttfont) {
-      Error("TTF::SetTextFont", "font file %s not found in path", fontname);
-      if (fgFontCount) {
-         Warning("TTF::SetTextFont", "using default font %s", fgFontName[0]);
-         fgCurFontIdx = 0;    // use font 0 (default font, set in ctor)
-         return 0;
-      } else {
-         return 1;
-      }
-   }
-
    FT_Face  tface = (FT_Face) 0;
 
-   if (FT_New_Face(fgLibrary, ttfont, 0, &tface)) {
+   if (FT_New_Face(fgLibrary, ttfont, ttindex, &tface)) {
       Error("TTF::SetTextFont", "error loading font %s", ttfont);
       delete [] ttfont;
       if (tface) FT_Done_Face(tface);
@@ -448,19 +568,20 @@
    delete [] ttfont;
 
    fgFontName[fgFontCount] = StrDup(basename);
+   fgFontIdx[fgFontCount]  = ttindex;
+   fgFontIta[fgFontCount]  = italic;
    fgCurFontIdx            = fgFontCount;
    fgFace[fgCurFontIdx]    = tface;
    fgCharMap[fgCurFontIdx] = (FT_CharMap) 0;
    fgFontCount++;
 
-   if (italic) {
-      fgSymbItaFontIdx = fgCurFontIdx;
+   if ((italic & 1) != 0) {
       FT_Matrix slantMat;
       slantMat.xx = (1 << 16);
       slantMat.xy = ((1 << 16) >> 2);
       slantMat.yx = 0;
       slantMat.yy = (1 << 16);
-      FT_Set_Transform( fgFace[fgSymbItaFontIdx], &slantMat, NULL );
+      FT_Set_Transform( fgFace[fgCurFontIdx], &slantMat, nullptr );
    }
 
    return 0;
@@ -489,70 +610,50 @@
 
 void TTF::SetTextFont(Font_t fontnumber)
 {
-   // Added by cholm for use of DFSG - fonts - based on Kevins fix.
-   // Table of Microsoft and (for non-MSFT operating systems) backup
-   // FreeFont TTF fonts.
-   static const char *fonttable[][2] = {
-     { "Root.TTFont.0", "FreeSansBold.otf" },
-     { "Root.TTFont.1", "FreeSerifItalic.otf" },
-     { "Root.TTFont.2", "FreeSerifBold.otf" },
-     { "Root.TTFont.3", "FreeSerifBoldItalic.otf" },
-     { "Root.TTFont.4", "texgyreheros-regular.otf" },
-     { "Root.TTFont.5", "texgyreheros-italic.otf" },
-     { "Root.TTFont.6", "texgyreheros-bold.otf" },
-     { "Root.TTFont.7", "texgyreheros-bolditalic.otf" },
-     { "Root.TTFont.8", "FreeMono.otf" },
-     { "Root.TTFont.9", "FreeMonoOblique.otf" },
-     { "Root.TTFont.10", "FreeMonoBold.otf" },
-     { "Root.TTFont.11", "FreeMonoBoldOblique.otf" },
-     { "Root.TTFont.12", "symbol.ttf" },
-     { "Root.TTFont.13", "FreeSerif.otf" },
-     { "Root.TTFont.14", "wingding.ttf" },
-     { "Root.TTFont.15", "symbol.ttf" },
-     { "Root.TTFont.STIXGen", "STIXGeneral.otf" },
-     { "Root.TTFont.STIXGenIt", "STIXGeneralItalic.otf" },
-     { "Root.TTFont.STIXGenBd", "STIXGeneralBol.otf" },
-     { "Root.TTFont.STIXGenBdIt", "STIXGeneralBolIta.otf" },
-     { "Root.TTFont.STIXSiz1Sym", "STIXSiz1Sym.otf" },
-     { "Root.TTFont.STIXSiz1SymBd", "STIXSiz1SymBol.otf" },
-     { "Root.TTFont.STIXSiz2Sym", "STIXSiz2Sym.otf" },
-     { "Root.TTFont.STIXSiz2SymBd", "STIXSiz2SymBol.otf" },
-     { "Root.TTFont.STIXSiz3Sym", "STIXSiz3Sym.otf" },
-     { "Root.TTFont.STIXSiz3SymBd", "STIXSiz3SymBol.otf" },
-     { "Root.TTFont.STIXSiz4Sym", "STIXSiz4Sym.otf" },
-     { "Root.TTFont.STIXSiz4SymBd", "STIXSiz4SymBol.otf" },
-     { "Root.TTFont.STIXSiz5Sym", "STIXSiz5Sym.otf" },
-     { "Root.TTFont.ME", "DroidSansFallback.ttf" },
-     { "Root.TTFont.CJKMing", "DroidSansFallback.ttf" },
-     { "Root.TTFont.CJKGothic", "DroidSansFallback.ttf" }
+   static const char *fonttable[] = {
+      "freesans:bold",
+      "freeserif:italic",
+      "freeserif:bold",
+      "freeserif:bold:italic",
+      "freesans",
+      "freesans:italic",
+      "freesans:bold",
+      "freesans:bold:italic",
+      "freemono",
+      "freemono:italic",
+      "freemono:bold",
+      "freemono:bold:italic",
+      "standardsymbolsps",
+      "freeserif",
+      "dingbats",
+      "standardsymbolsps",
+      "stixgeneral",
+      "stixgeneral:italic",
+      "stixgeneral:bold",
+      "stixgeneral:bold:italic",
+      "stixsizeonesym",
+      "stixsizeonesym:bold",
+      "stixsizetwosym",
+      "stixsizetwosym:bold",
+      "stixsizethreesym",
+      "stixsizethreesym:bold",
+      "stixsizefoursym",
+      "stixsizefoursym:bold",
+      "stixsizefivesym",
+      "droidsansfallback:charset=4e00 0410",
+      "droidsansfallback:charset=4e00 0410",
+      "droidsansfallback:charset=4e00 0410"
    };
 
-   static int fontset = -1;
-   int        thisset = fontset;
-
    int fontid = fontnumber / 10;
    if (fontid < 0 || fontid > 31) fontid = 0;
 
-   if (thisset == -1) {
-      // try to load font (font must be in Root.TTFontPath resource)
-      // to see which fontset we have available
-      const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                          TROOT::GetTTFFontDir());
-      char *ttfont = gSystem->Which(ttpath, gEnv->GetValue(fonttable[fontid][0], fonttable[fontid][1]), kReadPermission);
-      if (ttfont) {
-         delete [] ttfont;
-         thisset = 0;
-      } else {
-         // try backup free font
-         thisset = 1;
-      }
-   }
    Int_t italic = 0;
-   if (fontid==15) italic = 1;
-   int ret = SetTextFont(gEnv->GetValue(fonttable[fontid][thisset], fonttable[fontid][1]), italic);
-   // Do not define font set is we're loading the symbol.ttf - it's
-   // the same in both cases.
-   if (ret == 0 && fontid != 12) fontset = thisset;
+   if (fontid==12) italic = 2;
+   if (fontid==14) italic = 2;
+   if (fontid==15) italic = 3;
+
+   SetTextFont(fonttable[fontid], italic);
 }
 
 ////////////////////////////////////////////////////////////////////////////////
--- a/graf2d/postscript/CMakeLists.txt
+++ b/graf2d/postscript/CMakeLists.txt
@@ -27,6 +27,7 @@
   LIBRARIES
     ZLIB::ZLIB
     mathtext
+    fontconfig
   DEPENDENCIES
     Graf
 )
--- a/graf2d/postscript/src/TPostScript.cxx
+++ b/graf2d/postscript/src/TPostScript.cxx
@@ -234,6 +234,7 @@
 #include <cctype>
 #include <cwchar>
 #include <fstream>
+#include <fontconfig/fontconfig.h>
 
 #include "strlcpy.h"
 #include "snprintf.h"
@@ -1585,56 +1586,60 @@
 
 void TPostScript::FontEmbed(void)
 {
-   static const char *fonttable[32][2] = {
-      { "Root.TTFont.0", "FreeSansBold.otf" },
-      { "Root.TTFont.1", "FreeSerifItalic.otf" },
-      { "Root.TTFont.2", "FreeSerifBold.otf" },
-      { "Root.TTFont.3", "FreeSerifBoldItalic.otf" },
-      { "Root.TTFont.4", "FreeSans.otf" },
-      { "Root.TTFont.5", "FreeSansOblique.otf" },
-      { "Root.TTFont.6", "FreeSansBold.otf" },
-      { "Root.TTFont.7", "FreeSansBoldOblique.otf" },
-      { "Root.TTFont.8", "FreeMono.otf" },
-      { "Root.TTFont.9", "FreeMonoOblique.otf" },
-      { "Root.TTFont.10", "FreeMonoBold.otf" },
-      { "Root.TTFont.11", "FreeMonoBoldOblique.otf" },
-      { "Root.TTFont.12", "symbol.ttf" },
-      { "Root.TTFont.13", "FreeSerif.otf" },
-      { "Root.TTFont.14", "wingding.ttf" },
-      { "Root.TTFont.15", "symbol.ttf" },
-      { "Root.TTFont.STIXGen", "STIXGeneral.otf" },
-      { "Root.TTFont.STIXGenIt", "STIXGeneralItalic.otf" },
-      { "Root.TTFont.STIXGenBd", "STIXGeneralBol.otf" },
-      { "Root.TTFont.STIXGenBdIt", "STIXGeneralBolIta.otf" },
-      { "Root.TTFont.STIXSiz1Sym", "STIXSiz1Sym.otf" },
-      { "Root.TTFont.STIXSiz1SymBd", "STIXSiz1SymBol.otf" },
-      { "Root.TTFont.STIXSiz2Sym", "STIXSiz2Sym.otf" },
-      { "Root.TTFont.STIXSiz2SymBd", "STIXSiz2SymBol.otf" },
-      { "Root.TTFont.STIXSiz3Sym", "STIXSiz3Sym.otf" },
-      { "Root.TTFont.STIXSiz3SymBd", "STIXSiz3SymBol.otf" },
-      { "Root.TTFont.STIXSiz4Sym", "STIXSiz4Sym.otf" },
-      { "Root.TTFont.STIXSiz4SymBd", "STIXSiz4SymBol.otf" },
-      { "Root.TTFont.STIXSiz5Sym", "STIXSiz5Sym.otf" },
-      { "Root.TTFont.ME", "DroidSansFallback.ttf" },
-      { "Root.TTFont.CJKMing", "DroidSansFallback.ttf" },
-      { "Root.TTFont.CJKCothic", "DroidSansFallback.ttf" }
+   static const char *fonttable[] = {
+      "freesans:bold",
+      "freeserif:italic",
+      "freeserif:bold",
+      "freeserif:bold:italic",
+      "freesans",
+      "freesans:italic",
+      "freesans:bold",
+      "freesans:bold:italic",
+      "freemono",
+      "freemono:italic",
+      "freemono:bold",
+      "freemono:bold:italic",
+      "standardsymbolsps",
+      "freeserif",
+      "dingbats",
+      "standardsymbolsps",
+      "stixgeneral",
+      "stixgeneral:italic",
+      "stixgeneral:bold",
+      "stixgeneral:bold:italic",
+      "stixsizeonesym",
+      "stixsizeonesym:bold",
+      "stixsizetwosym",
+      "stixsizetwosym:bold",
+      "stixsizethreesym",
+      "stixsizethreesym:bold",
+      "stixsizefoursym",
+      "stixsizefoursym:bold",
+      "stixsizefivesym",
+      "droidsansfallback:charset=4e00 0410",
+      "droidsansfallback:charset=4e00 0410",
+      "droidsansfallback:charset=4e00 0410"
    };
 
    PrintStr("%%IncludeResource: ProcSet (FontSetInit)@");
 
-   // try to load font (font must be in Root.TTFontPath resource)
-   const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                       TROOT::GetTTFFontDir());
-
    for (Int_t fontid = 1; fontid < 30; fontid++) {
       if (fontid != 15 && MustEmbed[fontid-1]) {
-         const char *filename = gEnv->GetValue(
-                                               fonttable[fontid][0], fonttable[fontid][1]);
-         char *ttfont = gSystem->Which(ttpath, filename, kReadPermission);
+         char *ttfont = nullptr;
+
+         FcPattern *pat, *match;
+         FcResult result;
+
+         pat = FcNameParse ((const FcChar8*) fonttable[fontid]);
+
+         FcConfigSubstitute (nullptr, pat, FcMatchPattern);
+         FcDefaultSubstitute (pat);
+         match = FcFontMatch (nullptr, pat, &result);
+         FcPatternGetString (match, FC_FILE, 0, (FcChar8**) &ttfont);
+
          if (!ttfont) {
-            Error("TPostScript::FontEmbed",
-                  "font %d (filename `%s') not found in path",
-                  fontid, filename);
+            Error("TPostScript::FontEmbed", "font %d not found",
+                  fontid);
          } else {
             if (FontEmbedType2(ttfont)) {
                // nothing
@@ -1643,12 +1648,13 @@
             } else if(FontEmbedType42(ttfont)) {
                // nothing
             } else {
-               Error("TPostScript::FontEmbed",
-                     "failed to embed font %d (filename `%s')",
-                     fontid, filename);
+               Error("TPostScript::FontEmbed", "failed to embed font %d)",
+                     fontid);
             }
-            delete [] ttfont;
          }
+
+         FcPatternDestroy (match);
+         FcPatternDestroy (pat);
       }
    }
    PrintStr("%%IncludeResource: font Times-Roman@");
@@ -2837,10 +2843,10 @@
       { "Root.PSFont.9", "/FreeMonoOblique" },
       { "Root.PSFont.10", "/FreeMonoBold" },
       { "Root.PSFont.11", "/FreeMonoBoldOblique" },
-      { "Root.PSFont.12", "/SymbolMT" },
+      { "Root.PSFont.12", "/StandardSymbolsL" },
       { "Root.PSFont.13", "/FreeSerif" },
-      { "Root.PSFont.14", "/Wingdings-Regular" },
-      { "Root.PSFont.15", "/SymbolMT" },
+      { "Root.PSFont.14", "/Dingbats" },
+      { "Root.PSFont.15", "/StandardSymbolsL" },
       { "Root.PSFont.STIXGen", "/STIXGeneral" },
       { "Root.PSFont.STIXGenIt", "/STIXGeneral-Italic" },
       { "Root.PSFont.STIXGenBd", "/STIXGeneral-Bold" },
--- a/graf3d/gl/CMakeLists.txt
+++ b/graf3d/gl/CMakeLists.txt
@@ -208,6 +208,7 @@
     ${GL2PS_LIBRARIES}
     ${X11_LIBRARIES}
     RGlew
+    fontconfig
   DEPENDENCIES
     Hist
     Gui
--- a/graf3d/gl/src/TGLFontManager.cxx
+++ b/graf3d/gl/src/TGLFontManager.cxx
@@ -36,6 +36,7 @@
 # include "FTGLBitmapFont.h"
 #endif
 
+#include <fontconfig/fontconfig.h>
 
 /** \class TGLFont
 \ingroup opengl
@@ -450,16 +451,120 @@
    FontMap_i it = fFontMap.find(TGLFont(size, fileID, mode));
    if (it == fFontMap.end())
    {
-      TString ttpath, file;
-      ttpath = gEnv->GetValue("Root.TTGLFontPath", TROOT::GetTTFFontDir());
-      {
-         //For extenede we have both ttf and otf.
-         char *fp = gSystem->Which(ttpath, fileID < fgExtendedFontStart ?
-                                   ((TObjString*)fgFontFileArray[fileID])->String() + ".ttf" :
-                                   ((TObjString*)fgFontFileArray[fileID])->String());
-         file = fp;
-         delete [] fp;
+      TString file;
+
+      TString fontname = fileID < fgExtendedFontStart ?
+         ((TObjString*)fgFontFileArray[fileID])->String() + ".ttf" :
+         ((TObjString*)fgFontFileArray[fileID])->String();
+
+      FcPattern *pat = nullptr, *match;
+      FcResult result;
+
+      if (strcmp(fontname, "timesi.ttf") == 0 ||
+          strcmp(fontname, "FreeSerifItalic.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freeserif:italic");
+      }
+      else if (strcmp(fontname, "timesbd.ttf") == 0 ||
+               strcmp(fontname, "FreeSerifBold.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freeserif:bold");
+      }
+      else if (strcmp(fontname, "timesbi.ttf") == 0 ||
+               strcmp(fontname, "FreeSerifBoldItalic.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freeserif:bold:italic");
+      }
+      else if (strcmp(fontname, "arial.ttf") == 0 ||
+               strcmp(fontname, "FreeSans.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freesans");
+      }
+      else if (strcmp(fontname, "ariali.ttf") == 0 ||
+               strcmp(fontname, "FreeSansOblique.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freesans:italic");
+      }
+      else if (strcmp(fontname, "arialbd.ttf") == 0 ||
+               strcmp(fontname, "FreeSansBold.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freesans:bold");
+      }
+      else if (strcmp(fontname, "arialbi.ttf") == 0 ||
+               strcmp(fontname, "FreeSansBoldOblique.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freesans:bold:italic");
+      }
+      else if (strcmp(fontname, "cour.ttf") == 0 ||
+               strcmp(fontname, "FreeMono.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freemono");
+      }
+      else if (strcmp(fontname, "couri.ttf") == 0 ||
+               strcmp(fontname, "FreeMonoOblique.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freemono:italic");
+      }
+      else if (strcmp(fontname, "courbd.ttf") == 0 ||
+               strcmp(fontname, "FreeMonoBold.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freemono:bold");
+      }
+      else if (strcmp(fontname, "courbi.ttf") == 0 ||
+               strcmp(fontname, "FreeMonoBoldOblique.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freemono:bold:italic");
+      }
+      else if (strcmp(fontname, "symbol.ttf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "standardsymbolsps");
+      }
+      else if (strcmp(fontname, "times.ttf") == 0 ||
+               strcmp(fontname, "FreeSerif.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "freeserif");
+      }
+      else if (strcmp(fontname, "wingding.ttf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "dingbats");
+      }
+      else if (strcmp(fontname, "STIXGeneral.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixgeneral");
       }
+      else if (strcmp(fontname, "STIXGeneralItalic.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixgeneral:italic");
+      }
+      else if (strcmp(fontname, "STIXGeneralBol.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixgeneral:bold");
+      }
+      else if (strcmp(fontname, "STIXGeneralBolIta.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixgeneral:bold:italic");
+      }
+      else if (strcmp(fontname, "STIXSiz1Sym.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizeonesym");
+      }
+      else if (strcmp(fontname, "STIXSiz1SymBol.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizeonesym:bold");
+      }
+      else if (strcmp(fontname, "STIXSiz2Sym.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizetwosym");
+      }
+      else if (strcmp(fontname, "STIXSiz2SymBol.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizetwosym:bold");
+      }
+      else if (strcmp(fontname, "STIXSiz3Sym.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizethreesym");
+      }
+      else if (strcmp(fontname, "STIXSiz3SymBol.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizethreesym:bold");
+      }
+      else if (strcmp(fontname, "STIXSiz4Sym.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizefoursym");
+      }
+      else if (strcmp(fontname, "STIXSiz4SymBol.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizefoursym:bold");
+      }
+      else if (strcmp(fontname, "STIXSiz5Sym.otf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "stixsizefivesym");
+      }
+      else if (strcmp(fontname, "DroidSansFallback.ttf") == 0) {
+         pat = FcNameParse ((const FcChar8*) "droidsansfallback:charset=4e00 0410");
+      }
+
+      FcConfigSubstitute (nullptr, pat, FcMatchPattern);
+      FcDefaultSubstitute (pat);
+      match = FcFontMatch (nullptr, pat, &result);
+      char *ttfnt;
+      FcPatternGetString (match, FC_FILE, 0, (FcChar8**) &ttfnt);
+      file = ttfnt;
+      FcPatternDestroy (match);
+      FcPatternDestroy (pat);
 
       FTFont* ftfont = 0;
       switch (mode)
--- a/graf3d/gl/src/TGLText.cxx
+++ b/graf3d/gl/src/TGLText.cxx
@@ -32,6 +32,8 @@
 # include "FTGLBitmapFont.h"
 #endif
 
+#include <fontconfig/fontconfig.h>
+
 #define FTGL_BITMAP  0
 #define FTGL_PIXMAP  1
 #define FTGL_OUTLINE 2
@@ -176,27 +178,35 @@
 {
    int fontid = fontnumber / 10;
 
-   const char *fontname=0;
-   if (fontid == 0)  fontname = "arialbd.ttf";
-   if (fontid == 1)  fontname = "timesi.ttf";
-   if (fontid == 2)  fontname = "timesbd.ttf";
-   if (fontid == 3)  fontname = "timesbi.ttf";
-   if (fontid == 4)  fontname = "arial.ttf";
-   if (fontid == 5)  fontname = "ariali.ttf";
-   if (fontid == 6)  fontname = "arialbd.ttf";
-   if (fontid == 7)  fontname = "arialbi.ttf";
-   if (fontid == 8)  fontname = "cour.ttf";
-   if (fontid == 9)  fontname = "couri.ttf";
-   if (fontid == 10) fontname = "courbd.ttf";
-   if (fontid == 11) fontname = "courbi.ttf";
-   if (fontid == 12) fontname = "symbol.ttf";
-   if (fontid == 13) fontname = "times.ttf";
-   if (fontid == 14) fontname = "wingding.ttf";
-
-   // try to load font (font must be in Root.TTFontPath resource)
-   const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                       TROOT::GetTTFFontDir());
-   char *ttfont = gSystem->Which(ttpath, fontname, kReadPermission);
+   static const char *fonttable[] = {
+      "freesans:bold",
+      "freeserif:italic",
+      "freeserif:bold",
+      "freeserif:bold:italic",
+      "freesans",
+      "freesans:italic",
+      "freesans:bold",
+      "freesans:bold:italic",
+      "freemono",
+      "freemono:italic",
+      "freemono:bold",
+      "freemono:bold:italic",
+      "standardsymbolsps",
+      "freeserif",
+      "dingbats",
+   };
+
+   char *ttfont;
+
+   FcPattern *pat, *match;
+   FcResult result;
+
+   pat = FcNameParse ((const FcChar8*) fonttable[fontid]);
+
+   FcConfigSubstitute (nullptr, pat, FcMatchPattern);
+   FcDefaultSubstitute (pat);
+   match = FcFontMatch (nullptr, pat, &result);
+   FcPatternGetString (match, FC_FILE, 0, (FcChar8**) &ttfont);
 
    if (fGLTextFont) delete fGLTextFont;
 
@@ -204,7 +214,9 @@
 
    fGLTextFont = new FTGLPolygonFont(ttfont);
 
+   FcPatternDestroy (match);
+   FcPatternDestroy (pat);
+
    if (!fGLTextFont->FaceSize(1))
       Error("SetGLTextFont","Cannot set FTGL::FaceSize");
-   delete [] ttfont;
 }
--- a/gui/gui/src/TGApplication.cxx
+++ b/gui/gui/src/TGApplication.cxx
@@ -81,20 +81,12 @@
    gROOT->SetBatch(kFALSE);
 
    if (strcmp(appClassName, "proofserv")) {
-      const char *ttpath = gEnv->GetValue("Root.TTFontPath",
-                                          TROOT::GetTTFFontDir());
-      char *ttfont = gSystem->Which(ttpath, "arialbd.ttf", kReadPermission);
-      // Added by cholm for use of DFSG - fonts - based on fix by Kevin
-      if (!ttfont)
-         ttfont = gSystem->Which(ttpath, "FreeSansBold.ttf", kReadPermission);
-      if (ttfont && gEnv->GetValue("Root.UseTTFonts", 1)) {
+      if (gEnv->GetValue("Root.UseTTFonts", 1)) {
          TPluginHandler *h;
          if ((h = gROOT->GetPluginManager()->FindHandler("TVirtualX", "x11ttf")))
             if (h->LoadPlugin() == -1)
                Info("TGApplication", "no TTF support");
       }
-
-      delete [] ttfont;
    }
 
    // Create the canvas colors early so they are allocated before
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -659,7 +659,6 @@
                          PATTERN "system.rootdaemonrc" EXCLUDE
                          PATTERN "root.mimes" EXCLUDE
                          PATTERN "*.in" EXCLUDE)
-  install(DIRECTORY fonts/  DESTINATION ${CMAKE_INSTALL_FONTDIR})
   install(DIRECTORY icons/  DESTINATION ${CMAKE_INSTALL_ICONDIR})
   install(DIRECTORY macros/ DESTINATION ${CMAKE_INSTALL_MACRODIR})
   if(http)

# To check ROOT's currently available build options, check cmake/modules/RootBuildOptions.cmake

# CMake settings
set (CMAKE_CXX_STANDARD 20 CACHE STRING "" FORCE)
set (CMAKE_INSTALL_SYSCONFDIR "/etc/root-framework" CACHE PATH "" FORCE)
set (CMAKE_INSTALL_DATADIR "share/root-framework" CACHE PATH "" FORCE)
set (CMAKE_INSTALL_DOCDIR "share/doc/root-framework" CACHE PATH "" FORCE)

# enable ccache for faster rebuilds
set (ccache ON CACHE BOOL "" FORCE)

# install settings
set (fail-on-missing ON CACHE BOOL "" FORCE)
set (gnuinstall ON CACHE BOOL "" FORCE)
set (rpath ON CACHE BOOL "" FORCE)
set (runtime_cxxmodules ON CACHE BOOL "" FORCE)
set (soversion ON CACHE BOOL "" FORCE)
set (shared ON CACHE BOOL "" FORCE)

# enable heavily patched builtin llvm component
set (builtin_cling ON CACHE BOOL "" FORCE)  # build cling as standalone package with clang-repl when possible
set (builtin_clang ON CACHE BOOL "" FORCE)  # still needs to patches (possible contact person: hahnjo@hahnjo.de)
set (builtin_llvm OFF CACHE BOOL "" FORCE)

# disable other builtins when possible
set (builtin_afterimage OFF CACHE BOOL "" FORCE)
set (builtin_cfitsio OFF CACHE BOOL "" FORCE)
set (builtin_cppzmq OFF CACHE BOOL "" FORCE)
set (builtin_davix OFF CACHE BOOL "" FORCE)
set (builtin_fftw3 OFF CACHE BOOL "" FORCE)
set (builtin_freetype OFF CACHE BOOL "" FORCE)
set (builtin_ftgl OFF CACHE BOOL "" FORCE)
set (builtin_gl2ps OFF CACHE BOOL "" FORCE)
set (builtin_glew OFF CACHE BOOL "" FORCE)
set (builtin_gsl OFF CACHE BOOL "" FORCE)
set (builtin_gtest OFF CACHE BOOL "" FORCE)
set (builtin_lz4 OFF CACHE BOOL "" FORCE)
set (builtin_lzma OFF CACHE BOOL "" FORCE)
set (builtin_nlohmannjson OFF CACHE BOOL "" FORCE)
set (builtin_openssl OFF CACHE BOOL "" FORCE)
set (builtin_openui5 OFF CACHE BOOL "" FORCE)  # to be packaged
set (builtin_pcre OFF CACHE BOOL "" FORCE)
set (builtin_tbb OFF CACHE BOOL "" FORCE)
set (builtin_unuran OFF CACHE BOOL "" FORCE)
set (builtin_vc OFF CACHE BOOL "" FORCE)
set (builtin_vdt OFF CACHE BOOL "" FORCE)
set (builtin_veccore OFF CACHE BOOL "" FORCE)
set (builtin_xrootd OFF CACHE BOOL "" FORCE)
set (builtin_xxhash OFF CACHE BOOL "" FORCE)
set (builtin_zeromq OFF CACHE BOOL "" FORCE)
set (builtin_zlib OFF CACHE BOOL "" FORCE)
set (builtin_zstd OFF CACHE BOOL "" FORCE)

# optional features
set (arrow OFF CACHE BOOL "" FORCE)  # requires apache-arrow, see https://bugs.debian.org/970021
set (asimage ON CACHE BOOL "" FORCE)
set (cefweb OFF CACHE BOOL "" FORCE)
set (clad OFF CACHE BOOL "" FORCE)  # package clad, send PR upstream for builtin_clad=OFF option?
set (cuda OFF CACHE BOOL "" FORCE)
set (cudnn OFF CACHE BOOL "" FORCE)
set (daos OFF CACHE BOOL "" FORCE)  # requires DAOS
set (dataframe ON CACHE BOOL "" FORCE)
set (davix ON CACHE BOOL "" FORCE)
set (dcache ON CACHE BOOL "" FORCE)
set (fcgi ON CACHE BOOL "" FORCE)
set (fftw3 ON CACHE BOOL "" FORCE)
set (fitsio ON CACHE BOOL "" FORCE)
set (fortran ON CACHE BOOL "" FORCE)
set (gdml ON CACHE BOOL "" FORCE)
set (gviz ON CACHE BOOL "" FORCE)
set (http ON CACHE BOOL "" FORCE)
set (imt ON CACHE BOOL "" FORCE)
set (mathmore ON CACHE BOOL "" FORCE)
set (minuit2_mpi OFF CACHE BOOL "" FORCE)  # not thread safe, see https://github.com/root-project/root/pull/12970
set (minuit2_omp OFF CACHE BOOL "" FORCE)  # same as above
set (mlp ON CACHE BOOL "" FORCE)
set (mpi ON CACHE BOOL "" FORCE)
set (mysql ON CACHE BOOL "" FORCE)
set (odbc ON CACHE BOOL "" FORCE)
set (opengl ON CACHE BOOL "" FORCE)
set (pgsql ON CACHE BOOL "" FORCE)
set (proof ON CACHE BOOL "" FORCE)
set (pyroot ON CACHE BOOL "" FORCE)
set (pythia8 OFF CACHE BOOL "" FORCE)  # requires pythia8
set (qt5web ON CACHE BOOL "" FORCE)
set (qt6web ON CACHE BOOL "" FORCE)
set (r ON CACHE BOOL "" FORCE)
set (roofit ON CACHE BOOL "" FORCE)
set (roofit_multiprocess OFF CACHE BOOL "" FORCE)  # requires zmq_ppoll, see https://bugs.debian.org/1011073
set (root7 ON CACHE BOOL "" FORCE)
set (shadowpw ON CACHE BOOL "" FORCE)
set (spectrum ON CACHE BOOL "" FORCE)
set (sqlite ON CACHE BOOL "" FORCE)
set (ssl ON CACHE BOOL "" FORCE)
set (tmva ON CACHE BOOL "" FORCE)
set (tmva-cpu ON CACHE BOOL "" FORCE)
set (tmva-gpu OFF CACHE BOOL "" FORCE)
set (tmva-pymva ON CACHE BOOL "" FORCE)
set (tmva-rmva ON CACHE BOOL "" FORCE)
set (tmva-sofie ON CACHE BOOL "" FORCE)
set (unfold ON CACHE BOOL "" FORCE)
set (unuran ON CACHE BOOL "" FORCE)
set (uring ON CACHE BOOL "" FORCE)
set (vc ON CACHE BOOL "" FORCE)  # overwritten in rules for non-supported platforms
set (vdt ON CACHE BOOL "" FORCE)
set (veccore ON CACHE BOOL "" FORCE)
set (vecgeom ON CACHE BOOL "" FORCE)
set (webgui OFF CACHE BOOL "" FORCE)  # requires openui5, see https://bugs.debian.org/1003321
set (x11 ON CACHE BOOL "" FORCE)
set (xml ON CACHE BOOL "" FORCE)
set (xrootd ON CACHE BOOL "" FORCE)
